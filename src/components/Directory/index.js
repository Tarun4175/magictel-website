import React from 'react'
import EarPhone from './../../assets/earphone.jpg'
import EarBud from './../../assets/earbud.jpeg'
import PowerBank from './../../assets/powerbank.jpg'
import './styles.scss';

const Directory = props => {
  return (
    <div className="directory">
      <div className="wrap">
       <div
       className="item"
       style={{
         backgroundImage: `url(${EarPhone})`
       }}>
         <a>Buy Earphone</a>
       </div>

       <div
       className="item"
       style={{
         backgroundImage: `url(${EarBud})`
       }}>
         <a>Buy EarBud</a>
       </div>

       <div
       className="item"
       style={{
         backgroundImage: `url(${PowerBank})`
       }}>
         <a>Buy PowerBank</a>
       </div>

       <div
       className="item"
       style={{
         backgroundImage: `url(${PowerBank})`
       }}>
         <a>Buy PowerBank</a>
       </div>

       <div
       className="item"
       style={{
         backgroundImage: `url(${PowerBank})`
       }}>
         <a>Buy PowerBank</a>
       </div>

       <div
       className="item"
       style={{
         backgroundImage: `url(${PowerBank})`
       }}>
         <a>Buy PowerBank</a>
       </div>

       <div
       className="item"
       style={{
         backgroundImage: `url(${EarBud})`
       }}>
         <a>Buy EarBud</a>
       </div>

       <div
       className="item"
       style={{
         backgroundImage: `url(${EarPhone})`
       }}>
         <a>Buy Earphone</a>
       </div>

       

       </div>
    </div>
  );
};

export default Directory
